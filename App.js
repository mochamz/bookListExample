import React, { Component } from 'react';
import { Modal, Text, TouchableOpacity, View } from 'react-native';
import { Button, Col, Container, Content, Form, Grid, Header, Icon, Input, Item, Label, Row, Title } from 'native-base';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            formTitle: null,
            formBook: null,
            formCategory: null,
            formAuthor: null,
            formTotalPages: null,
            bookList: []
        }
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    closeModal() {
        this.setState({ formTitle: null, formBook: null, formCategory: null, formAuthor: null, formTotalPages: null, modalVisible: false });
    }

    saveBook() {
        let bookList = this.state.bookList;
        bookList.push({ book: this.state.formBook, category: this.state.formCategory, author: this.state.formAuthor, totalPages: this.state.formTotalPages });
        this.setState({ bookList: bookList });
        this.closeModal();
    }

    removeBook(index) {
        let bookList = this.state.bookList;
        bookList.splice(index, 1);
        this.setState({ bookList: bookList });
    }

    bookList() {
        return this.state.bookList.map((item, index) => {
            return (
                <TouchableOpacity style={{ padding: 10, backgroundColor: "#FFFFFF", borderBottomWidth: 1, borderColor: '#999999' }} onPress={() => this.removeBook(index)} key={index}>
                    <Grid>
                        <Row>
                            <Col style={{ flex: 1 }}><Text style={{ fontSize: 18, fontWeight: 'bold' }}>Name</Text></Col>
                            <Col style={{ flex: 2 }}><Text style={{ fontSize: 18 }}>{item.book}</Text></Col>
                        </Row>
                        <Row>
                            <Col style={{ flex: 1 }}><Text style={{ fontSize: 18, fontWeight: 'bold' }}>Category</Text></Col>
                            <Col style={{ flex: 2 }}><Text style={{ fontSize: 18 }}>{item.category}</Text></Col>
                        </Row>
                        <Row>
                            <Col style={{ flex: 1 }}><Text style={{ fontSize: 18, fontWeight: 'bold' }}>Author</Text></Col>
                            <Col style={{ flex: 2 }}><Text style={{ fontSize: 18 }}>{item.author}</Text></Col>
                        </Row>
                        <Row>
                            <Col style={{ flex: 1 }}><Text style={{ fontSize: 18, fontWeight: 'bold' }}>Total Pages</Text></Col>
                            <Col style={{ flex: 2 }}><Text style={{ fontSize: 18 }}>{item.totalPages}</Text></Col>
                        </Row>
                    </Grid>
                </TouchableOpacity>
            );
        });
    }

    render() {
        return (
            <Container>
                {/* Main Header */}
                <Header androidStatusBarColor={'#942674'} style={{ backgroundColor: '#B32E8D' }}>
                    <Grid>
                        <Col style={{ flex: 4, alignSelf: 'center', flexDirection: 'row', justifyContent: 'flex-start' }}>
                            <Title style={{ fontSize: 20 }}>Book List</Title>
                        </Col>
                        <Col style={{ flex: 4, alignSelf: 'center', flexDirection: 'row', justifyContent: 'flex-end' }}>
                            <TouchableOpacity onPress={() => this.setState({ modalVisible: true, formTitle: 'Add New Book' })}>
                                <Text style={{ color: '#FFFFFF' }}>Add Book</Text>
                            </TouchableOpacity>
                        </Col>
                    </Grid>
                </Header>

                {/* Main Content */}
                <Content>
                    {this.bookList()}
                </Content>

                {/* Form Modal */}
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.closeModal();
                    }}>

                    {/* Header Form */}
                    <Header androidStatusBarColor={'#942674'} style={{ backgroundColor: '#B32E8D' }}>
                        <Grid>
                            <Col style={{ flex: 1, alignSelf: 'center' }}>
                                <TouchableOpacity
                                    onPress={() => this.closeModal()} disabled={this.state.formDisabled}>
                                    <Icon name='arrow-back' style={{ color: '#FFF' }} />
                                </TouchableOpacity>
                            </Col>
                            <Col style={{ flex: 7, alignSelf: 'center', flexDirection: 'row', justifyContent: 'flex-start' }}>
                                <Title style={{ fontSize: 20 }}>{this.state.formTitle}</Title>
                            </Col>
                        </Grid>
                    </Header>

                    {/* Content Form */}
                    <Content>
                        <Form>
                            <Item fixedLabel>
                                <Label>Book Name</Label>
                                <Input value={this.state.formBook} onChangeText={(text) => this.setState({ formBook: text ? text : null })} />
                            </Item>
                            <Item fixedLabel>
                                <Label>Category</Label>
                                <Input value={this.state.formCategory} onChangeText={(text) => this.setState({ formCategory: text ? text : null })} />
                            </Item>
                            <Item fixedLabel>
                                <Label>Author</Label>
                                <Input value={this.state.formAuthor} onChangeText={(text) => this.setState({ formAuthor: text ? text : null })} />
                            </Item>
                            <Item fixedLabel last>
                                <Label>Total Pages</Label>
                                <Input value={this.state.formTotalPages} onChangeText={(text) => this.setState({ formTotalPages: text ? text : null })} keyboardType='numeric' />
                            </Item>
                        </Form>
                        <View style={{ padding: 10, marginTop: 20 }}>
                            <Button block style={{ backgroundColor: '#B32E8D' }} onPress={() => { this.saveBook() }}>
                                <Text style={{ color: '#fff' }}>Save</Text>
                            </Button>
                        </View>
                    </Content>
                </Modal>
            </Container >
        );
    }
}
